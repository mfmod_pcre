#include "config.h"
#include <mailfromd/mfmod.h>
#include <mailfromd/exceptions.h>
#include <mailutils/mailutils.h>
#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>
#include <ctype.h>

struct pcre_regex {
	pcre2_code *re;
	uint32_t ovcount;//FIXME?
	unsigned refcount;
	int istemp;
};

static struct pcre_regex *rxtab;
static size_t rxcount;

static int
regex_find_unused(void)
{
	size_t i;
	
	for (i = 0; i < rxcount; i++) {
		if (rxtab[i].re == NULL)
			return i;
	}

	rxtab = mu_2nrealloc(rxtab, &rxcount, sizeof(rxtab[0]));
	memset(&rxtab[i], 0, (rxcount - i) * sizeof(rxtab[0]));
	return i;
}

static struct pcre_regex *
regex_get(long n)
{
	if (n >= 0 && n < rxcount && rxtab[n].re != NULL)
		return &rxtab[n];
	return NULL;
}

static void
regex_free(struct pcre_regex *re)
{
	pcre2_code_free(re->re);
	memset(re, 0, sizeof(*re));
}

static inline void
regex_ref(struct pcre_regex *re)
{
	re->refcount++;
}

static void
regex_unref(struct pcre_regex *re)
{
	if (--re->refcount == 0 && re->istemp)
		regex_free(re);
}

struct pcre_match {
	long rxn;
	char *subject;
	pcre2_match_data *match;
	//FIXME?
};

static struct pcre_match *matchtab;
static size_t matchcount;

static int
match_find_unused(void)
{
	size_t i;
	
	for (i = 0; i < matchcount; i++) {
		if (matchtab[i].subject == NULL)
			return i;
	}

	matchtab = mu_2nrealloc(matchtab, &matchcount, sizeof(matchtab[0]));
	memset(&matchtab[i], 0, (matchcount - i) * sizeof(matchtab[0]));
	return i;
}

static struct pcre_match *
match_get(long n)
{
	if (n >= 0 && n < matchcount && matchtab[n].subject != NULL)
		return &matchtab[n];
	return NULL;
}

/*
 * pcre_compile() - compiles a regular expression.
 * Arguments:
 *   0     string  regular expression
 * Returns:
 *   Descriptor of the compiled expression.
 */
int
mfmod_pcre_compile(long count, MFMOD_PARAM *param, MFMOD_PARAM *retval)
{
	int options = 0;
	int errcode;
	PCRE2_SIZE erroff;
	pcre2_code *re;
	struct pcre_regex *rp;
	int n;

	ASSERT_ARGCOUNT(retval, count, 1);
	ASSERT_ARGTYPE(param, retval, 0, mfmod_string);

	re = pcre2_compile((PCRE2_SPTR8)param->string, PCRE2_ZERO_TERMINATED,
			   options, &errcode, &erroff, NULL);
	if (re == NULL) {
		static char const here[] = "<-- HERE";
		char errbuf[120];

		switch (pcre2_get_error_message(errcode, (PCRE2_UCHAR8*)errbuf,
						sizeof errbuf)) {
		case PCRE2_ERROR_NOMEMORY:
			return mfmod_error(retval, mfe_regcomp, "out of memory");
		case PCRE2_ERROR_BADDATA:
			strncpy(errbuf, "bad error code", sizeof(errbuf)-1);
			break;
		}
		erroff++;
		return mfmod_error(retval, mfe_regcomp,
				   "%s; marked by %s in %*.*s%s %s",
				   errbuf, here,
				   //FIXME: typecasts
				   (int)erroff, (int)erroff, param->string,
				   here,
				   param->string + erroff);
	}

	n = regex_find_unused();
	rp = &rxtab[n];

	rp->re = re;

	retval->type = mfmod_number;
	retval->number = n;
	
	return 0;
}

/*
 * pcre_match() - match a string against a regular expression
 * Arguments:
 *   [0]    number or string  regular expression handle or expression itself
 *   [1]    string            string to match
 * Returns:
 *   -1 if string doesn't match
 *   non-negative match descriptor if it does match.
 */
int
mfmod_pcre_match(long count, MFMOD_PARAM *param, MFMOD_PARAM *retval)
{
	struct pcre_regex *re;
	struct pcre_match *match;
	long rxn;
	int rc;
	long n;
	pcre2_match_data *md;
	
	ASSERT_ARGCOUNT(retval, count, 2);
	ASSERT_ARGTYPE(param, retval, 1, mfmod_string);

	if (param[0].type == mfmod_number) {
		rxn = param[0].number;
		if ((re = regex_get(rxn)) == NULL) {
			return mfmod_error(retval, mfe_range,
					   "%s", "invalid regexp descriptor");
		}
	} else if (param[0].type == mfmod_string) {
		int rc;
		MFMOD_PARAM r;
		
		rc = mfmod_pcre_compile(1, param, &r);
		if (rc) {
			*retval = r;
			return rc;
		}
		rxn = r.number;
		if ((re = regex_get(rxn)) == NULL) {
			return mfmod_error(retval, mfe_range,
					   "%s", "invalid regexp descriptor");
		}
		re->istemp = 1;
	} else {
		return mfmod_error(retval, mfe_inval,
				   "bad type of argument #1: expected number or string, but given %s",
				   param[0].type);
	}

	md = pcre2_match_data_create_from_pattern(re->re, NULL);
	if (md == NULL) {
		return mfmod_error(retval, mfe_regcomp,
				   "out of memory allocating match data");
	}
	
	regex_ref(re);

	rc = pcre2_match(re->re,
			 (PCRE2_SPTR8) param[1].string, PCRE2_ZERO_TERMINATED,
			 0, 0, md, NULL);

	if (rc == PCRE2_ERROR_NOMATCH) {
		retval->type = mfmod_number;
		retval->number = -1;
		pcre2_match_data_free(md);
		rc = 0;
	} else if (rc < 0) {
		rc = mfmod_error(retval, mfe_regcomp, "pcre2_match error: %d", rc);
	} else {	
		n = match_find_unused();
		match = &matchtab[n];

		match->rxn = rxn;
		regex_ref(re);
		match->subject = mu_strdup(param[1].string);
		match->match = md;
		
		retval->type = mfmod_number;
		retval->number = n;

		rc = 0;
	}
	
	regex_unref(re);
	
	return rc;
}

/*
 * pcre_substring() - get nth captured string
 * Arguments:
 *   [0]   number             match handle
 *   [1]   number or string   capture number or name
 * Returns:
 *   captured string
 */
int
mfmod_pcre_substring(long count, MFMOD_PARAM *param, MFMOD_PARAM *retval)
{
	struct pcre_match *mp;
	long n;
	size_t len;
	PCRE2_SIZE *ovector;
	
	ASSERT_ARGCOUNT(retval, count, 2);
	ASSERT_ARGTYPE(param, retval, 0, mfmod_string);
	
	if ((mp = match_get(param[0].number)) == NULL) {
		return mfmod_error(retval, mfe_range,
				   "%s", "invalid match descriptor");
	}

	if (param[1].type == mfmod_number) {
		n = param[1].number;
	} else if (param[1].type == mfmod_string) {
		struct pcre_regex *re;

		if ((re = regex_get(mp->rxn)) == NULL) {
			return mfmod_error(retval, mfe_range,
					   "%s", "invalid regexp descriptor");
		}
		
		n = pcre2_substring_number_from_name(re->re, (PCRE2_SPTR8) param[1].string);
		if (n < 0) {
			return mfmod_error(retval, mfe_range,
					   "%s", "named substring not found");
		}
	} else {
		return mfmod_error(retval, mfe_inval,
				   "bad type of argument #2: expected number or string, but given %s",
				   param[1].type);
	}		

	if (!(n >= 0 && n < pcre2_get_ovector_count(mp->match))) {
		return mfmod_error(retval, mfe_inval,
				   "%s", "backreference out of allowed range");
	}

	n *= 2;
	ovector = pcre2_get_ovector_pointer(mp->match);
	len = ovector[n+1] - ovector[n];
	retval->type = mfmod_string;
	retval->string = mu_alloc(len + 1);
	memcpy(retval->string, mp->subject + ovector[n], len);
	retval->string[len] = 0;

	return 0;
}
	
struct buffer {
	char *base;
	size_t size;
	size_t len;
};

static inline void
buffer_assert(struct buffer *buf, size_t n)
{
	while (buf->len + n > buf->size)
		buf->base = mu_2nrealloc(buf->base, &buf->size, 1);
}

static inline void
buffer_append(struct buffer *buf, char const *s, size_t n)
{
	buffer_assert(buf, n);
	memcpy(buf->base + buf->len, s, n);
	buf->len += n;
}

static inline int
getnum(struct pcre_regex *re, char *input, size_t *len, char **errtext)
{
	long n;

	*errtext = NULL;
	if (isdigit(input[1])) {
		n = input[1] - '0';
		*len = 2;
	} else if (input[1] == '{') {
		char *p;
		errno = 0;
		n = strtol(input + 2, &p, 10);
		if (errno || *p != '}')
			return -1;
		*len = p - input + 1;
	} else if (input[1] == '+' || input[2] == '{') {
		char *name;
		char *start = input + 3;
		size_t namelen;
		char *p = strchr(start, '}');
		if (p == NULL)
			return -1;
		namelen = p - start;
		*len = namelen + 4;
		name = malloc(namelen + 1);
		if (!name) {
			*errtext = "out of memory";
			return -1;
		}
		memcpy(name, start, namelen);
		name[namelen] = 0;

		n = pcre2_substring_number_from_name(re->re, (PCRE2_SPTR8) name);
		free(name);
		if (n < 0) {
			*errtext = "named substring not found";
			return -1;
		}
	} else
		n = -1;

	return n;
}
	
/*
 * pcre_expand() - expand backreferences in string
 * Arguments:
 *   [0]    number    match handle
 *   [1]    string    string to expand
 * Returns:
 *   Expanded string
 */
int
mfmod_pcre_expand(long count, MFMOD_PARAM *param, MFMOD_PARAM *retval)
{
	struct pcre_match *mp;
	struct pcre_regex *re;
	char *input;
	struct buffer buf = { NULL, 0, 0 };

	ASSERT_ARGCOUNT(retval, count, 2);
	ASSERT_ARGTYPE(param, retval, 0, mfmod_number);
	ASSERT_ARGTYPE(param, retval, 1, mfmod_string);
	
        if ((mp = match_get(param[0].number)) == NULL) {
		return mfmod_error(retval, mfe_range,
				   "%s", "invalid match descriptor");
	}

	if ((re = regex_get(mp->rxn)) == NULL) {
		return mfmod_error(retval, mfe_range,
				   "%s", "invalid regexp descriptor");
	}
	
	input = param[1].string;
	
	while (*input) {
		size_t len = strcspn(input, "\\$");
		int n;
		
		if (len > 0) {
			buffer_append(&buf, input, len);
		}
		input += len;
		
		if (input[0] == '\\') {
			input++;
			if (*input) {
				buffer_append(&buf, input, 1);
				input++;
			} else 
				buffer_append(&buf, input-1, 1);
		} else if (input[0] == '$') {
			char *err;
			if ((n = getnum(re, input, &len, &err)) == -1) {
				if (err) {
					free(buf.base);
					retval->type = mfmod_string;
					retval->string = strdup(err);
					return -1;
				}
				buffer_append(&buf, input, 1);
				input++;
			} else if (n >= 0 && n < pcre2_get_ovector_count(mp->match)) {
				PCRE2_SIZE *ovector;
				
				ovector = pcre2_get_ovector_pointer(mp->match);
				
				input += len;
				n <<= 1;
				buffer_append(&buf,
					      mp->subject + ovector[n], 
					      ovector[n+1] - ovector[n]);
			} else {
				buffer_append(&buf, input, len);
				input += len;
			}
		}
	}
	buffer_append(&buf, "", 1);

	retval->type = mfmod_string;
	retval->string = buf.base;

	return 0;
}

/*
 * pcre_match_free() - free a match structure
 * Arguments:
 *   [0]    number    match handle
 * Returns:
 *   Nothing
 */
int
mfmod_pcre_match_free(long count, MFMOD_PARAM *param, MFMOD_PARAM *retval)
{
	struct pcre_match *mp;
	struct pcre_regex *re;

	ASSERT_ARGCOUNT(retval, count, 1);
	ASSERT_ARGTYPE(param, retval, 0, mfmod_number);
	
	if ((mp = match_get(param[0].number)) == NULL)
		return 0;

	if ((re = regex_get(mp->rxn)) == NULL) {
		return mfmod_error(retval, mfe_range,
				   "%s", "invalid regexp descriptor");
	}
	regex_unref(re);

	free(mp->subject);
	pcre2_match_data_free(mp->match);
	memset(mp, 0, sizeof(*mp));

	retval->type = mfmod_number;
	retval->number = 0;

	return 0;
}

struct msg_stack {
	mu_message_t msg;
	mu_coord_t crd;
	struct msg_stack *prev;
};

static struct msg_stack *
msg_stack_push(struct msg_stack **stk)
{
	struct msg_stack *ent = mu_calloc(1, sizeof(*ent));
	ent->prev = *stk;
	*stk = ent;
	return ent;
}

static struct msg_stack *
msg_stack_pop(struct msg_stack **stk)
{
	if (*stk) {
		struct msg_stack *top = *stk;
		*stk = top->prev;
		top->prev = NULL;
		return top;
	}
	return NULL;
}

static void
msg_stack_free(struct msg_stack **stk)
{
	while (*stk) {
		struct msg_stack *prev = (*stk)->prev;
		free((*stk)->crd);
		free(*stk);
		*stk = prev;
	}
}

extern int mfmod_message_body_stream(mu_message_t msg, char const *charset,
				     mu_stream_t *pstr);

static int
grep_stack_entry(struct msg_stack *ent, struct pcre_regex *re, pcre2_match_data *match, char const *charset)
{
	mu_stream_t str, ostr;
	int rc;
	mu_off_t size;
	char *text;

	rc = mfmod_message_body_stream(ent->msg, charset, &str);
	if (rc)
		return -1;

	mu_memory_stream_create(&ostr, MU_STREAM_RDWR);
	rc = mu_stream_copy(ostr, str, 0, NULL);
	if (rc) {
		mu_diag_funcall(MU_DIAG_ERROR, "mu_stream_copy", NULL, rc);
		mu_stream_unref(str);
		mu_stream_unref(ostr);
		return -1;
	}
	mu_stream_size(ostr, &size);
	text = mu_alloc(size + 1);
	mu_stream_seek(ostr, 0, MU_SEEK_SET, NULL);
	mu_stream_read(ostr, text, size, NULL);
	text[size] = 0;

	rc = pcre2_match(re->re,
			 (PCRE2_SPTR8) text, PCRE2_ZERO_TERMINATED,
			 0, 0, match, NULL);

	free(text);
	mu_stream_unref(str);
	mu_stream_unref(ostr);
	
	if (rc == PCRE2_ERROR_NOMATCH)
		return 0;
	else if (rc < 0)
		return -1;
	return 1;
}

/*
 * pcre_message()
 * Arguments:
 *   [0]    number or string     regular expression handle or string
 *   [1]    message              message to search
 *   [2]    string               (optional) character set
 * Returns:
 *   Boolean value.  True if regexp matched, false otherwise.
 */
int
mfmod_pcre_message(long count, MFMOD_PARAM *param, MFMOD_PARAM *retval)
{
	struct pcre_regex *re;
	pcre2_match_data *match;
	struct msg_stack *stk = NULL;
	char *charset = NULL;
	int rc;
	char *found = NULL;

	if (!(count == 2 || count == 3))
		return mfmod_error(retval, mfe_inval,
				   "%s", "bad number of arguments");
	ASSERT_ARGTYPE(param, retval, 1, mfmod_message);
	if (count == 3)
		ASSERT_ARGTYPE(param, retval, 2, mfmod_string);
			
	if (param[0].type == mfmod_number) {
		if ((re = regex_get(param[0].number)) == NULL)
			return mfmod_error(retval, mfe_range,
					   "%s", "invalid regexp descriptor");
	} else if (param[0].type == mfmod_string) {
		MFMOD_PARAM r;
		
		rc = mfmod_pcre_compile(1, param, &r);
		if (rc) {
			*retval = r;
			return rc;
		}
		if ((re = regex_get(r.number)) == NULL) {
			return mfmod_error(retval, mfe_range,
					   "%s", "invalid regexp descriptor");
		}
		re->istemp = 1;
	} else {
		return mfmod_error(retval, mfe_inval,
				   "bad type of argument #2: expected number or string, but given %s",
				   param[1].type);
	}

	match = pcre2_match_data_create_from_pattern(re->re, NULL);
	if (!match)
		return mfmod_error(retval, mfe_regcomp,
				   "out of memory allocating match data");

	regex_ref(re);

	msg_stack_push(&stk);
	stk->msg = param[1].message;
	mu_coord_alloc(&stk->crd, 1);
	stk->crd[1] = 1;

	retval->type = mfmod_number;
	retval->number = 0;
	
	while (stk) {
		struct msg_stack *ent = msg_stack_pop(&stk);
		int ismime;
		
		rc = mu_message_is_multipart(ent->msg, &ismime);
		if (rc) {
			mu_diag_funcall(MU_DIAG_ERROR,
					"mu_message_is_multipart", NULL, rc);
			break;
		} else if (ismime) {
			size_t i, nparts;
			rc = mu_message_get_num_parts(ent->msg, &nparts);
			if (rc) {
				mu_diag_funcall(MU_DIAG_ERROR, "mu_message_get_num_parts",
						NULL, rc);
				break;
			}

			for (i = 1; i <= nparts; i++) {
				struct msg_stack *newent = msg_stack_push(&stk);
				mu_coord_alloc(&newent->crd, mu_coord_length(ent->crd) + 1);
				memcpy(&newent->crd[1], &ent->crd[1], ent->crd[0] * sizeof(ent->crd[0]));
				newent->crd[newent->crd[0]] = i;
				rc = mu_message_get_part(ent->msg, i, &newent->msg);
				if (rc) {
					mu_diag_funcall(MU_DIAG_ERROR, "mu_message_get_part", NULL, rc);
					goto end;
				}
			}
		} else {
			rc = grep_stack_entry(ent, re, match, charset);
			if (rc == -1)
				break;
			else if (rc == 1) {
				found = mu_coord_string(ent->crd);
				rc = 0;
				break;
			}
		}
	}
end:
	msg_stack_free(&stk);
	regex_unref(re);
	pcre2_match_data_free(match);

	if (rc)
		return -1;

	retval->type = mfmod_string;
	retval->string = found ? found : strdup("");
	
	return 0;
}
